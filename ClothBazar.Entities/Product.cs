﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClothBazar.Entities
{
    public class Product:BaseEntity
    {
        public Category Category { get; set; }
        public decimal Price { get; set; }
        public string Size { get; set; }
    }
}
