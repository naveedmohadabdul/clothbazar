﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClothBazar.Entities
{
    public class Category:BaseEntity
    {
        public List<Product> Products { get; set; }
    }
}
